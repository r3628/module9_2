#!/bin/bash

cd ./terraform
terraform apply -auto-approve
cd ../ansible
sleep 2
source ./tf_ansible_vars
ansible-playbook play.yml

